package br.com.teste.leilao.teste;

import static io.restassured.RestAssured.*;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.Test;

import br.com.teste.leiloes.modelo.Usuario;
import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import static org.hamcrest.Matchers.*;
public class UsuariosWSTest {

	@Test
	public void deveRetornarListaDeUsuarios() {
		
		XmlPath path = 
				given()
				.header("Accept", "application/xml")
				.get("http://localhost:8080/usuarios?_format=xml").andReturn().xmlPath();

		Usuario usuario1 = path.getObject("list.usuario[0]", Usuario.class);
		Usuario usuario2 = path.getObject("list.usuario[1]", Usuario.class);

		Usuario esperado1 = new Usuario(1L, "Mauricio Aniche", "mauricio.aniche@caelum.com.br");
		Usuario esperado2 = new Usuario(2L, "Guilherme Silveira", "guilherme.silveira@caelum.com.br");
		
		System.out.println(esperado1);
		System.out.println(usuario1);

		assertEquals(esperado1, usuario1);
		assertEquals(esperado2, usuario2);
	}
	
	@Test
	public void deveRetornarUsuarioPeloID() {
		JsonPath path = 
				given()
		.header("Accept", "application/json")
		.param("usuario.id", 1)
		.get("http://localhost:8080/usuarios/show")
		.andReturn()
		.jsonPath();
		
		Usuario usuario = path.getObject("usuario", Usuario.class);
		
		Usuario esperado = new Usuario(1L, "Mauricio Aniche", "mauricio.aniche@caelum.com.br");
		
		assertEquals(esperado, usuario);
	}
	
	
}
